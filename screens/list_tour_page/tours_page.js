let list=[
    {
        id: 1,
        img:"/assets/images/phoCoDongVan1.jpg",
        location:"Phố cổ Đồng Văn",
        city:"Hà Giang",
    } ,
    {
        id: 2,
        img:"/assets/images/CatCat.JPG",
        location:"Bản Cát Cát",
        city:"Sapa"
    },
    {
        id: 3,
        img:"/assets/images/MangDen.jpg",
        location:"Rừng Thông Măng Đen",
        city:"Kon Tum"
    },{
        id: 4,
        img:"/assets/images/daoBinhHung.jpg",
        location:"Đảo Bình Hưng",
        city:"Khánh Hòa"
    } ,
    {
        id: 5,
        img:"/assets/images/PhuocHai.jpg",
        location:"Làng Phước Hải",
        city:"Vũng Tàu"
    },
    {
        id: 6,
        img:"/assets/images/CoTo.jpg",
        location:"Núi Cô Tô",
        city:"Tri Tôn"
    }
]
list.forEach(e=>{

    $("#list").append(
        `
            <a href="http://127.0.0.1:5500/screens/tour_detail_page/tour_detail_page.html" class="location-card" id="navigate${e?.id}" >
                <img class="location-img" src=${e?.img} alt="City"></div>
                <div class="location-content">
                    <p class="location-name">${e?.location}</p>
                    <p class="location-city">${e?.city}</p>
                </div>
            </a>
        `
    )
})

list.forEach(e=>{
    document.getElementById(`navigate${e?.id}`).addEventListener('click', ()=>{
        console.log('hihii');
        localStorage.setItem("id",e?.id);
    });
})