const carousel = document.querySelector('.carousel')
const imageItems = document.querySelectorAll('.carousel > *')
const widthToScroll = carousel.children[0].offsetWidth + 24
const arrowLeft = document.querySelector('.prev')
const arrowRight = document.querySelector('.next')
const imageLength = imageItems.length
const perView = 3
let totalScroll = 0
const delayTime = 2500

console.log(widthToScroll)

// arrowBtns.forEach(btn => {
//     btn.addEventListener("click", () => {
//         console.log(btn.id)
//         carousel.scrollLeft += btn.id === "prev" ? -widthToScroll : widthToScroll
//         console.log("click")
//     })
// })

carousel.style.setProperty('--per-view', perView)
for(let i = 0; i < perView; i++) {
    carousel.insertAdjacentHTML('beforeend', imageItems[i].outerHTML)
}

let autoScroll = setInterval(scrolling, delayTime)

function scrolling() {
    totalScroll--
    if (totalScroll <- (imageLength)) {
        clearInterval(autoScroll)
        totalScroll = -1
        carousel.style.transition = '0s'
        carousel.style.left = '0'
        autoScroll = setInterval(scrolling, delayTime)
    }
    console.log(totalScroll);
    const widthEl = document.querySelector('.carousel > :first-child').offsetWidth + 24
    carousel.style.left = `${totalScroll * widthEl}px`
    carousel.style.transition = '.8s'

}

arrowLeft.addEventListener('click', () => {
    if(totalScroll<0){
        console.log(totalScroll);
        const widthEl = document.querySelector('.carousel > :first-child').offsetWidth + 24
        totalScroll =totalScroll+1;
        carousel.style.left = `${totalScroll*widthEl}px`;
        carousel.style.transition = '.8s'
    }
    
})

arrowRight.addEventListener('click', () => {
    if(totalScroll>-imageLength){
        const widthEl = document.querySelector('.carousel > :first-child').offsetWidth + 24
        totalScroll =totalScroll-1;
        carousel.style.left = `${totalScroll*widthEl}px`;
        carousel.style.transition = '.8s'
    }    
})

// arrowLeft.onmousemove = function() {
//     console.log()
// }
