let id=localStorage.getItem("id");
// console.log(id);

let list = [
    {
        id: 1,
        location_name: "Phố cổ Đồng Văn",
        city_name: "Hà Giang",
        img:"/assets/images/phoCoDongVan1.jpg",
        description: "Tọa lạc tại thị trấn Đồng Văn thuộc huyện Đồng Văn, tỉnh Hà Giang, phố cổ Đồng Văn hiện lên với vẻ đẹp nhuốm màu thời gian. Đây là nơi sinh sống của nhiều đồng bào dân tộc thiểu số như Mông, Hoa, Tày, Dao, Nùng, Kinh và đó là lý do tạo nên sự đa dạng văn hóa tại nơi đây. Được hình thành từ đầu thế kỷ 20, phố cổ Đồng Văn sở hữu những ngôi nhà có tuổi đời lên đến hàng trăm năm. Đặc biệt, những ngôi nhà được thiết kế kết hợp giữa văn hóa Việt và lối kiến trúc Trung Hoa tạo nên vẻ thu hút, ấn tượng. Đến với Đồng Văn, bạn còn được trải nghiệm khí hậu mát mẻ, thưởng thức cảnh sắc và đồ ăn ngon.",
        special: [
            "Chiêm ngưỡng hương sắc tại cánh đồng hoa Tam giác mạch",
            "Đi dạo quanh các con đường và ngắm nhìn các ngôi nhà cổ kính",
            "Trải nghiệm cuộc sống thường ngày cùng với đồng bào dân tộc tại đây",
            "Khám phá khu chợ tại phố cổ Đồng Văn",
            "Mua quà lưu niệm được làm thủ công và gia vị, nguyên liệu đặc sản Hà Giang"
        ],
        food: [
            {
                food_name: "Cơm lam",
                food_img: "/assets/images/comLam.jpg",
            },

            {
                food_name: "Thịt trâu gác bếp",
                food_img: "/assets/images/thitTrauGacBep.jpg",
            },

            {
                food_name: "Thắng cố",
                food_img: "/assets/images/thangCo.jpg",
            }
        ]
    },
    {
        id: 2,
        location_name: "Bản Cát Cát",
        city_name: "Sapa",
        img:"/assets/images/CatCat.JPG",
        description: "Cách trung tâm thị trấn Sapa khoảng hơn 2km, Bản Cát Cát là một bản nhỏ thuộc thung lũng Mường Hoa. Cách xa  thành phố xô bồ, Bản Cát Cát hiện lên với vẻ đẹp yên bình có núi rừng, ruộng lúa, suối nước và cuộc sống bình yên. Đến với nơi đây, bạn được tận hưởng khí hậu mát mẻ của vùng núi cao Tây Bắc và chiêm ngưỡng vẻ đẹp đặc sắc của các loài hoa. Đặc biệt, Bản Cát Cát vào mùa đông còn được bao phủ bởi tuyết trắng xóa.",
        "special": [
            "Bạn có thể thuê trang phục của đồng bào dân tộc để chụp những bộ ảnh kỷ niệm.",
            "Mua các món quà lưu niệm là những mặt hàng thủ công mỹ nghệ như: bông tai, túi, trang phục,...",
            "Ghé thăm các gian nhà truyền thống để tìm hiểu về đời sống sinh hoạt của người bản địa.",
            "Khám phá các khu chợ của người dân tộc",
            "Check-in tại các địa điểm nổi tiếng như: thác Cát Cát, Ruộng bậc thang, bánh xe nước chảy, Suối Hoa,... "
        ],
        food: [
            {
                food_name: "Rượu táo mèo",
                food_img: "/assets/images/ruouTaoMeo.jpg",
            },

            {
                food_name: "Cá suối nướng",
                food_img: "/assets/images/caSuoiNuong.jpg",
            },

            {
                food_name: "Gà đen",
                food_img: "/assets/images/gaDen.jpg",
            }
        ]
    },
    {
        id: 3,
        location_name: "Rừng Thông Măng Đen",
        img:"/assets/images/MangDen.jpg",
        city_name: "Kon Tum",
        description: "Măng Đen thuộc tỉnh Kon Tum nằm trong đại ngàn Tây Nguyên rộng lớn. Nằm trên đỉnh núi cao và được bao phủ bởi rừng cây, Măng Đen sở hữu khí hậu mát mẻ, không gian yên tĩnh và hoang sơ. Đến với Măng Đen bạn có thể đắm mình vào cảnh sắc thiên nhiên hùng vĩ, chiêm ngưỡng vẻ đẹp của của các loài hoa như: hoa dã quỳ, hoa đào, hoa cẩm tú cầu,... Lúc này đây, tâm hồn của bạn như được chữa lành, đồng điệu và hòa vào nhịp thở của thiên nhiên.",
        special: [
            "Đi dạo xung qua những rừng cây thông, con đường đất đỏ hay đồi cà phê.",
            "Săn mây và cắm trại trên đỉnh núi Ngọc Lễ",
            "Check - in tại Hồ Đắc Ke",
            "Thắp hương tại Chùa Khánh Lâm"
        ],
        food: [
            {
                food_name: "Lẩu xuyên tiêu",
                food_img: "/assets/images/lauXuyenTieu.jpg",
            },

            {
                food_name: "Gỏi lá",
                food_img: "/assets/images/goiLa.jpg",
            },

            {
                food_name: "Rượu sim rừng",
                food_img: "/assets/images/ruoiSimRung.jpg",
            }
        ]
    },
    {
        id: 4,
        location_name: "Đảo Bình Hưng",
        city_name: "Khánh Hòa",
        img:"/assets/images/daoBinhHung.jpg",
        description: "Tọa lạc tại Nha Trang, Bình Hưng là một hòn đảo nhỏ thuộc bộ ba đảo Tam Bình trong vịnh nước sâu Cam Ranh. Đảo Bình Hưng hiện lên với vẻ đẹp hoang sơ và cuốn hút. Bãi cát trắng mịn, làn nước trong xanh, không gian yên bình là những gì bạn tìm thấy tại đây. Bên cạnh đó, khi đến với đảo Bình Hưng bạn còn có thể hòa mình vào cuộc sống của người dân nơi đây. Đặc biệt, đến với đảo bạn sẽ được thưởng thức những món ngon được chế biến từ hải sản tươi sống.",
        special: [
            "Ngắm nhìn làn nước trong xanh tại Bãi Kinh",
            "Dạo biển và check - in tại bãi biển Bình Tiên",
            "Tham quan ngọn Hải đăng Hòn chút có tuổi đời lên đến hơn 100 năm",
            "Chiêm ngưỡng Đảo Yến (Hang Tàu)"
        ],
        food: [
            {
                food_name: "Ốc",
                food_img: "/assets/images/cacLoaiOc.jpg",
            },

            {
                food_name: "Bánh căn mực, bánh căn trứng",
                food_img: "/assets/images/banh-can-nha-trang-4.jpg",
            },

            {
                food_name: "Bánh canh chả cá",
                food_img: "/assets/images/BanhCanhChaCa.jpg",
            }
        ]
    },
    {
        id: 5,
        location_name: "Làng Phước Hải",
        city_name: "Vũng Tàu",
        img:"/assets/images/PhuocHai.jpg",
        description: "Làng chài Phước Hải tọa lạc tại thị trấn Phước Hải, huyện Đất Đỏ, tỉnh Bà Rịa - Vũng Tàu. Đây là một làng chài đã tồn tại lâu đời và là nơi sinh sống, đánh bắt hải sản của nhiều hộ dân. Ngày nay, Phước Hải được biết đến là một bãi biển nghỉ mát xinh đẹp với nhịp sống rộn ràng, vui tươi. Đến với làng chài Phước Hải, bạn không chỉ được chiêm ngưỡng vẻ đẹp biển cả tươi mát, mà còn được thưởng thức hải sản tươi ngon và hòa vào nhịp sống yên ả của một làng chài nhỏ.",
        special: [
            "Dạo chơi và check in bờ biển làng Phước Hải",
            "Ngắm hoàng hôn và bình minh ở làng chài",
            "Trải nghiệm cuộc sống ngư dân ở Phước Hải",
            "Mua khô hải sản tại làng chài"
        ],
        food: [
            {
                food_name: "Lẩu súng",
                food_img: "/assets/images/lauLuong.jpg",
            },

            {
                food_name: "Bánh canh ghẹ",
                food_img: "/assets/images/banhCanhGhe.jpg",
            },

            {
                food_name: "Gỏi cá mai",
                food_img: "/assets/images/goiCaMai.jpg",
            }
        ]
    },
    {
        id: 6,
        location_name: "Núi Cô Tô",
        city_name: "Tri Tôn",
        img:"/assets/images/CoTo.jpg",
        description: "Tọa lạc tại huyện Tri Tôn, tỉnh An Giang, núi Cô Tô thuộc dãy Thất Sơn huyền bí. Núi Cô Tô gọi tắt là núi Tô hay Phụng Hoàng Sơn là một trong những ngọn núi được săn đón khi đến du lịch An Giang. Với độ cao 614m, từ đỉnh núi bạn có thể chiêm ngưỡng toàn bộ thị trấn Tri Tôn, những cánh đồng lúa bát ngát xen lẫn những hàng thốt nốt đặc trưng. Ngoài ra, ngọn núi này còn sở hữu hệ thống hang động ngầm rộng lớn với vẻ hoang sơ gắn liền với nhiều câu chuyện huyền bí. Đến đây, bạn sẽ cảm nhận được không khí trong lành, tươi mát, hòa mình cùng thiên nhiên và bình yên đến lạ thường.",
        special: [
            "Leo núi trải nghiệm vẻ đẹp hoang sơ",
            "Lên núi bằng xe ôm để trải nghiệm cảm giác mạo hiểm",
            "Tham quan miếu Bà Cố và những điện thờ linh thiêng",
            "Khám phá khu vực Sân Tiên, điện Năm Căn",
            "Check in chữ Tri Tôn tuyệt đẹp",
            "Quan sát toàn bộ thị trấn Tri Tôn, cánh đồng lúa vàng và những hàng thốt nốt",
            "Khám phá Dồ Hội Lớn và Dồ Hội Nhỏ",
            "Ngắm bình minh và săn mây sáng sớm"
        ],
        food: [
            {
                food_name: "Gà đốt",
                food_img: "/assets/images/gaDot.jpg",
            },

            {
                food_name: "Đu đủ đâm",
                food_img: "/assets/images/duDuDam.jpg",
            },

            {
                food_name: "Bò nướng",
                food_img: "/assets/images/boNuong.jpeg",
            }
        ]
    }
]


list.forEach(element => {
    if (element.id === Number(id)) {
        $("#detail").append(
            ` 
            <div class="detailimage">
                <img src="${element?.img}" alt="${element?.location_name}">
            </div>
            <div class="content">
                <h2>${element?.location_name}</h2>
                <h4>${element?.city_name}</h4>
                <div class="tongquan mgb-50">
                    <h3 class="title">Tổng quan</h3>
                    <p>${element?.description}</p>
                </div>
                <div class="noibat mgb-50">
                    <h3 class="title">Đặc điểm nổi bật</h3>
                    <ul>
                        ${element.special.map(e => 
                            `<li>${e}</li>`
                        ).join(' ')}
                    </ul>
                </div>
                <div class="food-container mgb-50">
                    <h3 class="title">Ẩm thực</h3>
                    <div class="food-section" id="foodList">
                            ${element.food.map(e => 
                                `<div class="food-card">
                                    <img class="food-img" src="${e.food_img}" alt="${e.food_name}">
                                    <div class="food-content">
                                        <p class="food-name">${e?.food_name}</p>
                                    </div>
                                </div>`
                            ).join(' ')}
                    </div> 
                </div>
            </div>
            `
        )
    }
}); 

